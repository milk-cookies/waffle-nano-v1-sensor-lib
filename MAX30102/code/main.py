from machine import I2C, Pin
import max30102
from machine import SPI, Pin
import utime
import st7789
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))

display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
display.init()
display.fill(st7789.color565(0, 0, 0))

m = max30102.MAX30102(0,1,100)
while True:
    red = m.read_fifo()
    utime.sleep(1)
    hr = int((red-230000)/1000+60)
    if hr<0:
        hr=0
    display.draw_string(20, 80, "H R", color=st7789.RED, bg=st7789.BLACK, size=6)
    display.draw_string(20, 150, "%s" %hr, color=st7789.YELLOW, bg=st7789.BLACK, size=8)