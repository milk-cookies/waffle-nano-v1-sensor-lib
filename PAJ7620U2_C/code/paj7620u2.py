from machine import SPI, Pin, I2C
import utime
import urandom

i2c = I2C(1, scl=Pin(1), sda=Pin(0), freq=400000)#构造IIC基本对象

#中间插入iic基本库与相关函数
class PAJ7620U2:
    #手势传感器的寄存器和变量
    GES_REACTION_TIME = .200
    GES_ENTRY_TIME = .400
    GES_QUIT_TIME = .800

    BANK0 = 0
    BANK1 = 1

    PAJ7620U2_ADDR_BASE = 0x00

    #寄存器分块选择
    PAJ7620U2_REGITER_BANK_SEL = (PAJ7620U2_ADDR_BASE + 0xEF)  # W

    #传感器 ID
    PAJ7620U2_ID = 115

    #寄存器分块 0
    PAJ7620U2_ADDR_SUSPEND_CMD = (PAJ7620U2_ADDR_BASE + 0x3)         # W
    PAJ7620U2_ADDR_GES_PS_DET_MASK_0 = (PAJ7620U2_ADDR_BASE + 0x41)  # RW
    PAJ7620U2_ADDR_GES_PS_DET_MASK_1 = (PAJ7620U2_ADDR_BASE + 0x42)  # RW
    PAJ7620U2_ADDR_GES_PS_DET_FLAG_0 = (PAJ7620U2_ADDR_BASE + 0x43)  # R
    PAJ7620U2_ADDR_GES_PS_DET_FLAG_1 = (PAJ7620U2_ADDR_BASE + 0x44)  # R
    PAJ7620U2_ADDR_STATE_INDICATOR = (PAJ7620U2_ADDR_BASE + 0x45)    # R
    PAJ7620U2_ADDR_PS_HIGH_THRESHOLD = (PAJ7620U2_ADDR_BASE + 0x69)  # RW
    PAJ7620U2_ADDR_PS_LOW_THRESHOLD = (PAJ7620U2_ADDR_BASE + 0x6A)   # RW
    PAJ7620U2_ADDR_PS_APPROACH_STATE = (PAJ7620U2_ADDR_BASE + 0x6B)  # R
    PAJ7620U2_ADDR_PS_RAW_DATA = (PAJ7620U2_ADDR_BASE + 0x6C)        # R

    #寄存器分块 1
    PAJ7620U2_ADDR_PS_GAIN = (PAJ7620U2_ADDR_BASE + 0x44)  # RW
    PAJ7620U2_ADDR_IDLE_S1_STEP_0 = (PAJ7620U2_ADDR_BASE + 0x67)  # RW
    PAJ7620U2_ADDR_IDLE_S1_STEP_1 = (PAJ7620U2_ADDR_BASE + 0x68)  # RW
    PAJ7620U2_ADDR_IDLE_S2_STEP_0 = (PAJ7620U2_ADDR_BASE + 0x69)  # RW
    PAJ7620U2_ADDR_IDLE_S2_STEP_1 = (PAJ7620U2_ADDR_BASE + 0x6A)  # RW
    PAJ7620U2_ADDR_OP_TO_S1_STEP_0 = (PAJ7620U2_ADDR_BASE + 0x6B)  # RW
    PAJ7620U2_ADDR_OP_TO_S1_STEP_1 = (PAJ7620U2_ADDR_BASE + 0x6C)  # RW
    PAJ7620U2_ADDR_OP_TO_S2_STEP_0 = (PAJ7620U2_ADDR_BASE + 0x6D)  # RW
    PAJ7620U2_ADDR_OP_TO_S2_STEP_1 = (PAJ7620U2_ADDR_BASE + 0x6E)  # RW
    PAJ7620U2_ADDR_OPERATION_ENABLE = (PAJ7620U2_ADDR_BASE + 0x72)  # RW

    #PAJ7620U2_REGITER_BANK_SEL
    PAJ7620U2_BANK0 = 0
    PAJ7620U2_BANK1 = 1

    #PAJ7620U2_ADDR_SUSPEND_CMD
    PAJ7620U2_I2C_WAKEUP  = 1
    PAJ7620U2_I2C_SUSPEND = 0

    #PAJ7620U2_ADDR_OPERATION_ENABLE
    PAJ7620U2_ENABLE  = 1
    PAJ7620U2_DISABLE = 0

    #ADC, delete
    REG_ADDR_RESULT   = 0x00
    REG_ADDR_ALERT    = 0x01
    REG_ADDR_CONFIG   = 0x02
    REG_ADDR_LIMITL   = 0x03
    REG_ADDR_LIMITH   = 0x04
    REG_ADDR_HYST     = 0x05
    REG_ADDR_CONVL    = 0x06
    REG_ADDR_CONVH    = 0x07


    GES_UP_FLAG              = 1 << 0
    GES_DOWN_FLAG            = 1 << 1
    GES_LEFT_FLAG            = 1 << 2
    GES_RIGHT_FLAG           = 1 << 3
    GES_FORWARD_FLAG         = 1 << 4
    GES_BACKWARD_FLAG        = 1 << 5
    GES_CLOCKWISE_FLAG       = 1 << 6
    GES_COUNT_CLOCKWISE_FLAG = 1 << 7
    GES_WAVE_FLAG            = 1 << 0

    #手势输出
    FORWARD        = 1
    BACKWARD       = 2
    RIGHT          = 3
    LEFT           = 4
    UP             = 5
    DOWN           = 6
    CLOCKWISE      = 7
    ANTI_CLOCKWISE = 8
    WAVE           = 9

    initRegisterArray = ([0xEF, 0x00],
                          [0x37, 0x07],
                          [0x38, 0x17],
                          [0x39, 0x06],
                          [0x42, 0x01],
                          [0x46, 0x2D],
                          [0x47, 0x0F],
                          [0x48, 0x3C],
                          [0x49, 0x00],
                          [0x4A, 0x1E],
                          [0x4C, 0x20],
                          [0x51, 0x10],
                          [0x5E, 0x10],
                          [0x60, 0x27],
                          [0x80, 0x42],
                          [0x81, 0x44],
                          [0x82, 0x04],
                          [0x8B, 0x01],
                          [0x90, 0x06],
                          [0x95, 0x0A],
                          [0x96, 0x0C],
                          [0x97, 0x05],
                          [0x9A, 0x14],
                          [0x9C, 0x3F],
                          [0xA5, 0x19],
                          [0xCC, 0x19],
                          [0xCD, 0x0B],
                          [0xCE, 0x13],
                          [0xCF, 0x64],
                          [0xD0, 0x21],
                          [0xEF, 0x01],
                          [0x02, 0x0F],
                          [0x03, 0x10],
                          [0x04, 0x02],
                          [0x25, 0x01],
                          [0x27, 0x39],
                          [0x28, 0x7F],
                          [0x29, 0x08],
                          [0x3E, 0xFF],
                          [0x5E, 0x3D],
                          [0x65, 0x96],
                          [0x67, 0x97],
                          [0x69, 0xCD],
                          [0x6A, 0x01],
                          [0x6D, 0x2C],
                          [0x6E, 0x01],
                          [0x72, 0x01],
                          [0x73, 0x35],
                          [0x74, 0x00],
                          [0x77, 0x01])

    def __init__(self, i2c):
        self.i2c = i2c
        self.addr = 115  #设置需要通信的从机（PAJ7629U2传感器）地址为115
        utime.sleep_us(700)

        self.PAJ7620U2SelectBank(0)
        self.PAJ7620U2SelectBank(0)

        initialization_value = i2c.readfrom_mem(115, 0x00, 1) #读取寄存器第一个数据
        if initialization_value != [32]:
            print("Error with sensor.")

        if initialization_value == [32]:
            print("wake-up finished")
            for i in self.initRegisterArray:
                i2c.write(115, bytes([i[0], i[1]]))

        self.PAJ7620U2SelectBank(0)
        print("PAJ7620U2 initialize register finished.")

    #将一个字节写入手势传感器上的寄存器
    def PAJ7620U2WriteReg(self, register, data):
        self.i2c.write(115, bytes([register, data]))

    #选择手势传感器上的寄存器分块
    def PAJ7620U2SelectBank(self, bank):
        if bank == self.BANK0:
            self.PAJ7620U2WriteReg(self.PAJ7620U2_REGITER_BANK_SEL, self.PAJ7620U2_BANK0)

    #从手势传感器的地址“register”开始读取长度为“length”的字节块
    def PAJ7620U2ReadReg(self, register, length):
        return self.i2c.readfrom_mem(115, register, length)

    #打印手势传感器的值
    def print_gesture(self):
        data = self.PAJ7620U2ReadReg(0x43, 1)[0]
        if data == self.GES_RIGHT_FLAG:
            i2c.sleep(self.GES_ENTRY_TIME)
            data = self.PAJ7620U2ReadReg(0x43, 1)[0]
            if data == self.GES_FORWARD_FLAG:
                print("Forward")
                i2c.sleep(self.GES_QUIT_TIME)
            elif data == self.GES_BACKWARD_FLAG:
                print("Backward")
                i2c.sleep(self.GES_QUIT_TIME)
            else:
                print("Right")

        elif data == self.GES_LEFT_FLAG:
            i2c.sleep(self.GES_ENTRY_TIME)
            data = self.PAJ7620U2ReadReg(0x43, 1)[0]
            if data == self.GES_FORWARD_FLAG:
                print("Forward")
                i2c.sleep(self.GES_QUIT_TIME)
            elif data == self.GES_BACKWARD_FLAG:
                print("Backward")
                i2c.sleep(self.GES_QUIT_TIME)
            else:
                print("Left")

        elif data == self.GES_UP_FLAG:
            i2c.sleep(self.GES_ENTRY_TIME)
            data = self.PAJ7620U2ReadReg(0x43, 1)[0]
            if data == self.GES_FORWARD_FLAG:
                print("Forward")
                i2c.sleep(self.GES_QUIT_TIME)
            elif data == self.GES_BACKWARD_FLAG:
                print("Backward")
                i2c.sleep(self.GES_QUIT_TIME)
            else:
                print("Up")

        elif data == self.GES_DOWN_FLAG:
            i2c.sleep(self.GES_ENTRY_TIME)
            data = self.PAJ7620U2ReadReg(0x43, 1)[0]
            if data == self.GES_FORWARD_FLAG:
                print("Forward")
                i2c.sleep(self.GES_QUIT_TIME)
            elif data == self.GES_BACKWARD_FLAG:
                print("Backward")
                i2c.sleep(self.GES_QUIT_TIME)
            else:
                print("Down")

        elif data == self.GES_FORWARD_FLAG:
            print("Forward")
            i2c.sleep(self.GES_QUIT_TIME)

        elif data == self.GES_BACKWARD_FLAG:
            print("Backward")
            i2c.sleep(self.GES_QUIT_TIME)

        elif data == self.GES_CLOCKWISE_FLAG:
            print("Clockwise")

        elif data == self.GES_COUNT_CLOCKWISE_FLAG:
            print("anti-clockwise")

        else:
            data1 = self.PAJ7620U2ReadReg(0x44, 1)[0]
            if (data1 == self.GES_WAVE_FLAG):
                print("wave")

    def return_gesture(self):

        data = self.PAJ7620U2ReadReg(0x43, 1)[0]
        if data:
            return data

        else:
            data1 = self.PAJ7620U2ReadReg(0x44, 1)[0]
            if (data1 == self.GES_WAVE_FLAG):
                return 9
        return 0
