from machine import I2C, Pin,SPI # 导入通信引脚及相关的库
import ds1307,utime,st7789 # 导入时钟模块,时间模块和屏幕驱动
i2c = I2C(1, sda=Pin(0), scl=Pin(1),freq=100000)    # 构造IIC对象，将1号引脚设置为scl，将0号引脚设置为sda，频率设为100000
week_day = ["Mon", "Tue", "Wed", "Thus", "Fri", "Sat", "Sun"]    # 设置星期列表便于下方将数字转换为星期
ds = ds1307.DS1307(i2c)    # 构造时钟对象
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))    # 构造spi对象
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))    # 构造屏幕控制对象
display.init()    # 屏幕初始化
display.fill(st7789.color565(255, 255, 255))    # 设置屏幕背景为白色

# 接下来的一部分为设置时间，设置完成后需删除，防止覆盖现有时间而产生错误
# print(ds.datetime()) # 显示当前DS1307模块的内置时间
# ds.halt(False) # 打开振荡器，让时间开始走动
# now=(2021,7,16,5,13,27,0,0)  # 接下来两句为设定时间
# ds.datetime(now)

while True:  # 不断刷新，获取实时数据
    date = ds.datetime()  # 获取时间和日历信息
    year = date[0]  # 年
    month = date[1]  # 月
    day = date[2]  # 日
    weekday = date[3]  # 星期
    hour = date[4]  # 小时
    minute = date[5]  # 分钟
    second = date[6]  # 秒

    # 统一格式
    year1_str = "%04d" % year
    month1_str = "%02d" % month
    day1_str = "%02d" % day
    time1_str = "%02d:%02d" % (hour, minute)
    second1_str = "%02d" % second

    display.draw_string(20, 15, "TIME", size=2, color=st7789.color565(112, 112, 112)) # 时间标签
    display.draw_string(40, 40, time1_str, size=6, color=st7789.color565(0, 0, 0))  # 在屏幕上显示时分
    display.draw_string(200, 70, second1_str, size=2, color=st7789.color565(67, 77, 120)) # 在屏幕上显示秒
    display.draw_string(5, 105, "  YEAR    MONTH   DAY", size=2, color=st7789.color565(209, 123, 17)) # 年月日标签
    display.draw_string(20, 130, year1_str + "  " + month1_str, size=3, color=st7789.color565(209, 123, 17)) # 在屏幕上显示年月
    display.draw_string(178, 130, day1_str, size=3, color=st7789.color565(209, 123, 17)) # 在屏幕上显示日(这里年月和日分开成两句是为了与上面的标签对齐)
    display.draw_string(10, 170, "  WEEK", size=2, color=st7789.color565(99, 194, 118)) # 星期标签
    display.draw_string(30, 195, week_day[weekday - 1], size=3, color=st7789.color565(99, 194, 118)) # 在屏幕上显示星期
    utime.sleep(1) # 每隔一秒刷新一次