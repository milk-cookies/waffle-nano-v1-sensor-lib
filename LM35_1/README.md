# LM35温度传感器

## 案例展示

​		将LM35连接至Waffle Nano上，导入LM35库和烧写main主程序后运行程序即可看到这样的场景

![](image/20210719173855.jpg)

## 物理连接

### 传感器选择

​		  传感器选择如下图所示的LM35温度传感器模组

![20210719173830](image/20210719173830.jpg)

### 传感器连线

​	   传感器与Waffle Nano之间的接线方式如下所示，其余接口都不连

| Waffle Nano | 传感器                                               |
| ----------- | ---------------------------------------------------- |
| 3V3         | VCC                                                  |
| GND         | GND                                                  |
| OUT         | G13（原则上是可以选择其他的引脚的，LM35库中选定G13） |

## 传感器库使用

​		  可以在code文件夹内获取LM35库（名称是LM35.py），将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`上

```python
import LM35
```

接着，定义一个新变量te接受LM35库中的temp函数的返回值

```
te=LM35.temp()
```

如果需要详尽的解释可以看LM35.py中的代码注释

## 案例代码复现

可以在code文件夹内获取main函数，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 上烧录进Waffle Nano上即可。

如果不明白可以看main.py中的代码注释