# 数字简谱播放

## 案例展示

​		基于十二平均律，将音乐简谱中的*音高*、*减增时线*、*升降符*、*变化音*和*单音*转换为传递给无源蜂鸣器的*PWM*频率和播放时长，并控制蜂鸣器将其播放出来。

[]: C:\Users\Marcus\Desktop\NumberedMusicalNotation\Image

注：由于芯片*PWM*最低输出频率*611*Hz，建议所有音高提高*1*个八度

## 物理连接

### 传感器选择

​		传感器选择如下图所示的无源蜂鸣器。

[]: C:\Users\Marcus\Desktop\NumberedMusicalNotation\Image

### 传感器接线

​		传感器与Waffle Nano之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连接状态。

| Waffle Nano | 传感器 |
| ----------- | ------ |
| V3.3        | V      |
| GND         | G      |
| IO2         | S      |

## 传感器库使用

​		可以获取

[]: C:\Users\Marcus\Desktop\NumberedMusicalNotation\Code	"NumberedMusicalNotation"

​		并将此库通过*Waffle Maker*的文件上传功能上传到*Waffle Nano*上。

​		我们在可以在主函数中使用以下代码导入此库。

> from NumberedMusicalNotation import *

​		在对象构造函数中，我们需要传入一个已经构造好的*PWM*对象。

> pwm = PWM(2, Pin(2))
>
> p = NumberedMusicalNotation(pwm)

​		根据简谱乐谱内容，编写数字简谱数组。格式如[M3，1.0]![image-20210719235829785](C:\Users\Marcus\AppData\Roaming\Typora\typora-user-images\image-20210719235829785.png)

​		第一格表示音符，第二格表示音符持续时长。（s开头表示升音）

​		数字乐谱存储格式为二维列表：

```
melody1 =[    #Moon River
    [M5, 2.0],
    [H2, 1.0],
    [H1, 2.0],
    [M7, 1.5],
    [M6, 0.5],
    [M5, 0.5],
    [M4, 0.5],
    [M5, 2.0],
    [M1, 1.0],
    [M7, 1.5],
    [M6, 0.5],
    [M5, 0.5],
    [M4, 0.5],
    [M5, 2.0],
    [M1, 1.0],
    [M2, 5.0],
    # 代码过长不全部展示，附带乐谱见music.py
]
```

​		每首音乐再存储在一个列表中：

```
melody =[
    melody1,
    melody2,
]
#本样例会无限循环播放列表内的音乐
```

​		循环遍历数字乐谱，并使用*beep*方法播放音乐。

## 案例代码复现

​		可以获取

[]: C:\Users\Marcus\Desktop\NumberedMusicalNotation\Code	"main.py"

函数，将其内容复制到*Waffle Maker*编辑器上传输给*Waffle Nano*,以复现此案例。