from machine import Pin, PWM
from NumberedMusicalNotation import *
pwm = PWM(2, Pin(2))
p = NumberedMusicalNotation(pwm)
index = 0
WhichMelody = 0
melody1 =[    #Moon River
    [M5, 2.0],
    [H2, 1.0],
    [H1, 2.0],
    [M7, 1.5],
    [M6, 0.5],
    [M5, 0.5],
    [M4, 0.5],
    [M5, 2.0],
    [M1, 1.0],
    [M7, 1.5],
    [M6, 0.5],
    [M5, 0.5],
    [M4, 0.5],
    [M5, 2.0],
    [M1, 1.0],
    [M2, 5.0],
    # 代码过长不全部展示，附带乐谱见music.py
]
melody2 =[      #天空之城
    [M6, 0.5],
    [M7, 0.5],
    [H1, 1.5],
    [M7, 0.5],
    [H1, 1.0],
    [H3, 1.0],
    [M7, 3.0],
    [0, 1.0],
    [M3, 0.5],
    [M3, 0.5],
    [M6, 1.5],
    [M5, 0.5],
    [M6, 1.0],
    [H1, 1.0],
    [M5, 2.0],
    # 代码过长不全部展示，附带乐谱见music.py
]
#添加播放音乐的方法：将music.py中的列表复制到这行的上方，并添加'melodyx'至下方列表内
melody =[
    melody1,
    melody2,
]
#本样例会无限循环播放列表内的音乐
while 1:
    while 1:
        p.beep(melody[WhichMelody][index][0], melody[WhichMelody][index][1])
        index = index + 1
        if index >= len(melody[WhichMelody]):
            index = 0
            break
    WhichMelody = WhichMelody + 1
    if WhichMelody == len(melody):
        WhichMelody = 0