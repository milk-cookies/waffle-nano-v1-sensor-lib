from machine import I2C, Pin
# i2c = I2C(1, sda=Pin(0), scl=Pin(1),freq=400000)
from utime import sleep
I2C_WRITE_ADDR = 0xAE
I2C_READ_ADDR = 0xAF

# register address-es
REG_INTR_STATUS_1 = 0x00
REG_INTR_STATUS_2 = 0x01

REG_INTR_ENABLE_1 = 0x02
REG_INTR_ENABLE_2 = 0x03

REG_FIFO_WR_PTR = 0x04
REG_OVF_COUNTER = 0x05
REG_FIFO_RD_PTR = 0x06
REG_FIFO_DATA = 0x07
REG_FIFO_CONFIG = 0x08

REG_MODE_CONFIG = 0x09
REG_SPO2_CONFIG = 0x0A
REG_LED1_PA = 0x0C

REG_LED2_PA = 0x0D
REG_PILOT_PA = 0x10
REG_MULTI_LED_CTRL1 = 0x11
REG_MULTI_LED_CTRL2 = 0x12

REG_TEMP_INTR = 0x1F
REG_TEMP_FRAC = 0x20
REG_TEMP_CONFIG = 0x21
REG_PROX_INT_THRESH = 0x30
REG_REV_ID = 0xFE
REG_PART_ID = 0xFF

class Max30102():
    def __init__(self,sda_pin,scl_pin,fre):
        # print("Channel: {0}, address: {1}".format(channel, address))
        self.i2c = I2C(1, sda=Pin(sda_pin), scl=Pin(scl_pin),freq=fre)
        self.address = 87

        self.reset()

        sleep(1)  # wait 1 sec

        # read & clear interrupt register (read 1 byte)
        reg_data = self.i2c.readfrom_mem(self.address, REG_INTR_STATUS_1, 1)
        # print("[SETUP] reset complete with interrupt register0: {0}".format(reg_data))
        self.setup()

    def shutdown(self):
        """
        Shutdown the device.
        """
        self.i2c.write(87, b'\x09\x80')

    def reset(self):
        """
        Reset the device, this will clear all settings,
        so after running this, run setup() again.
        """
        self.i2c.write(87, b'\x09\x40')


    def setup(self):
        """
        This will setup the device with the values written in sample Arduino code.
        """
        # INTR setting
        # 0xc0 : A_FULL_EN and PPG_RDY_EN = Interrupt will be triggered when
        # fifo almost full & new fifo data ready
        self.i2c.write(87, b' \x02\xc0')
        self.i2c.write(87, b'\x03\xc0')

        # FIFO_WR_PTR[4:0]
        self.i2c.write(87, b'\x04\xc0')
        # OVF_COUNTER[4:0]
        self.i2c.write(87, b'\x05\xc0')
        # FIFO_RD_PTR[4:0]
        self.i2c.write(87, b'\x06\xc0')

        # 0b 0100 1111
        # sample avg = 4, fifo rollover = false, fifo almost full = 17
        self.i2c.write(87, b'\x08\x4f')

        # 0x02 for read-only, 0x03 for SpO2 mode, 0x07 multimode LED
        self.i2c.write(87, b'\x09\x03')
        # 0b 0010 0111
        # SPO2_ADC range = 4096nA, SPO2 sample rate = 100Hz, LED pulse-width = 411uS
        self.i2c.write(87, b'\x0A\x27')

        # choose value for ~7mA for LED1
        self.i2c.write(87, b'\x0C\x40')
        # choose value for ~7mA for LED2
        self.i2c.write(87, b'\x0D\x40')
        # choose value fro ~25mA for Pilot LED
        self.i2c.write(87, b'\x10\x7f')

    def read_fifo(self):
        """
        This function will read the data register.
        """
        red_led = None
        ir_led = None

        # read 1 byte from registers (values are discarded)
        reg_INTR1 = self.i2c.readfrom_mem(87, REG_INTR_STATUS_1, 1)
        reg_INTR2 = self.i2c.readfrom_mem(87, REG_INTR_STATUS_2, 1)

        # read 6-byte data from the device
        d = self.i2c.readfrom_mem(87, REG_FIFO_DATA, 6)
        # mask MSB [23:18]
        red_led = (d[0] << 16 | d[1] << 8 | d[2]) & 0x03FFFF
        ir_led = (d[3] << 16 | d[4] << 8 | d[5]) & 0x03FFFF

        return red_led, ir_led

    def read_sequential(self):
        """
        This function will read the red-led and ir-led `amount` times.
        This works as blocking function.
        """
        red_buf = []
        ir_buf = []
        for i in range(100):
            red, ir = self.read_fifo()
            red_buf.append(red)
            ir_buf.append(ir)

        return red_buf, ir_buf
