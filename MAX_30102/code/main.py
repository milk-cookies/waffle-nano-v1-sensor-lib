from machine import I2C, Pin
import max30102
from utime import sleep
from machine import SPI, Pin


m = max30102.Max30102(0,1,100)

hr = 70
while True:
    red,ir = m.read_fifo()
    R = red/ir
    if red < 20000:
        print("please put your finger")
    else:
        hr = str(int(30/30000*(red-230000)+60))
        spo2 =str(int(-45.060*R*R+30.354 *R + 94.845+22))
        print(red,ir,spo2,hr)

        display.draw_string(10, 10,"hr = %s      " %hr,size=3,color=st7789.color565(0, 0, 0))
        display.draw_string(10, 50,"spo2 = %s    " %spo2,size=3,color=st7789.color565(0, 0, 0))
       # display.ST7789.fill_rect(10, 10, 200, 200,st7789.color565(255, 255, 255))