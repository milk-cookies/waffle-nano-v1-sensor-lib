from machine import UART, Pin,SPI
import st7789
import utime
def uart_init():
    uart = UART(2)  # 构造UART2串口
    uart.init(baudrate=9600, parity=None, tx=Pin(11), rx=Pin(12), timeout=10)  # uart初始化
    return uart

def convert_data(x):
    x1=''
    for i in x:
        if i == 'b':
            continue
        elif i == "'":
            continue
        if i == 'm':
            break
        x1 = x1 + i
    print(x1)
    return x1

def screen_init():
    spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
    display = st7789.ST7789(spi, 240, 240, reset=Pin(11, func=Pin.GPIO, dir=Pin.OUT),
                            dc=Pin(7, func=Pin.GPIO, dir=Pin.OUT))
    display.init()
    display.fill(st7789.color565(124, 205, 124))
    for i in range(1, 10):
        display.draw_string(20, 200 - i * 20, 'Welcome to', size=4, color=st7789.color565(0, 0, 0))
        display.fill_rect(20, 230 - i * 20, 220, 30, st7789.color565(124, 205, 124))
        utime.sleep_us(3200)
    for i in range(1, 10):
        display.draw_string(20, 200 - i * 17, 'Eye protection', size=3, color=st7789.color565(0, 255, 0))
        display.fill_rect(20, 230 - i * 17, 220, 60, st7789.color565(124, 205, 124))
        utime.sleep_us(1600)
    for i in range(1, 10):
        display.draw_string(20, 200 - i * 14 - 3, 'assistant!', size=4, color=st7789.color565(200, 0, 0))
        display.fill_rect(20, 230 - i * 14 - 3, 220, 30, st7789.color565(124, 205, 124))
        utime.sleep_us(800)
    for i in range(3):
        display.fill_circle(120, 180, 60, st7789.color565(250, 252, 0))
        display.draw_string(100, 160, str(3 - i), size=7, color=st7789.color565(255, 255, 255))
        utime.sleep(0.95)
    return display




