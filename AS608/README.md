# AS608指纹识别传感器



## 物理连接

### 传感器选择

    本文所指的传感器为如下图所示的AS608指纹识别传感器。


![](https://images.gitee.com/uploads/images/2021/0719/104906_172dece4_9429238.jpeg "AS608传感器.jpg")


    一般我们选择3.3V的电源来使传感器运行




### 传感器接线

  传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚处于不连接状态。

  （本处讲的只是本人调试的一种连接方式，其他一些链接方式也可以接受，但需要改一下主函数的初始化）
  

| Waffle Nano | 传感器  |
| ----------- | ------ |
| 3.3V        | 3.3V   |
| IO0         | 白色线  |
| IO5         | 黄色线  |
| GND         | GND    |


### 传感器接线示意图

  （本处讲的只是本人调试的一种连接方式，其他一些链接方式也可以接受，但需要改一下主函数的初始化）


  ![](https://images.gitee.com/uploads/images/2021/0719/105726_d8ea22f2_9429238.jpeg "基本链接串口.jpg")



## 传感器库使用

  该传感器在WaFFle中的库为[AS608.py](https://gitee.com/milk-cookies/waffle-nano-v1-sensor-lib/blob/master/AS608/code/AS608.py),下载此库，并通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`板子上，即可引用相应的函数库。


  引用该库的代码为

```python
from AS608 import uart_init, search
```

​   按照传感器，需要2个GPIO引脚（本人测试的时候采用IO5为rx接口，IO0为tx接口，已经写入了库函数里）。构造方法如下：

```python
uart = uart_init()
```

​   `uart`对象的`read()`方法返回的是你传送过去指令包的应答包格式，可以参照一下传感器的一些指令包[FUCTION.txt](https://gitee.com/milk-cookies/waffle-nano-v1-sensor-lib/blob/master/AS608/%E5%9F%BA%E6%9C%AC%E4%BA%A4%E4%BA%92%E5%8A%9F%E8%83%BD.txt)。
可以通过这个文本来增添更多功能，实现更深层次的交互~

```python
dd=uart.read()

print(dd)
```





## 基本功能实现

​本案例介绍了AS608的一些基本的交互功能，即实现该传感器最基本的功能，搜索指纹，显示是否匹配


搜索成功的话会显示如下图


![](https://images.gitee.com/uploads/images/2021/0721/142028_53da8d5a_9429238.jpeg "搜索成功质量也很高.jpg")



但是如果搜索不到指纹的话，你就会得到这样的反馈


![](https://images.gitee.com/uploads/images/2021/0721/142043_6cde9d52_9429238.jpeg "搜索失败.jpg")



  好啦，案例介绍到此结束，可以获取主函数[Main.py](https://gitee.com/milk-cookies/waffle-nano-v1-sensor-lib/blob/master/AS608/code/Main.py)了解一下工作机理，在已经上传过库文件的前提下将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 网页编译器上烧录给`Waffle Nano`，就可以成功复现此案例了~


  如果有啥疑惑的，可以看看后面的注释哦！
