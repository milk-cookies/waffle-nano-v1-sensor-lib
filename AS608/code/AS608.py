from machine import UART, Pin, SPI
import st7789
import utime

def uart_init():#端口初始化
    uart = UART(1)
    uart.init(baudrate=57600, parity=None, tx=Pin(0), rx=Pin(5))  # 构造串口，启用uart通信协议，这边只是提供一个样例
    return uart

def search(display,uart):#将手指放到传感器上，进行指纹搜索
    for i in range(0, 8):  # 指令（17）自动验证指纹（现场自动匹配）
        uart.write(b'\xEF\x01\xFF\xFF\xFF\xFF\x01\x00\x03\x11\x00\x15')
        utime.sleep(1)
        dd = uart.read()
        print(dd)#测试点
        if dd[9] == 0:  # 用户搜索成功，返回用户ID以及匹配度
            if dd[12] + dd[13] > 15:#搜索成功，匹配较高
                print("The search is a good match")
                print(str(dd[10] + dd[11])+" , Your match score is"+str(dd[12] + dd[13]))
            else:#搜索不成功，匹配度不是很高
                print("Your match is not good")
                print(str(dd[10] + dd[11])+" , Your match score is"+str(dd[12] + dd[13]))
            return 1
            break
        if i == 7:#搜索失败
            print("Search failure")
            return 0
            break
    utime.sleep(2)


print(1)#判断库是否调用正确的一个测试点