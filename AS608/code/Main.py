from machine import UART, Pin, SPI
import st7789
import  utime, network
from AS608 import search,uart_init

uart = uart_init()#uart初始化

spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
display = st7789.ST7789(spi, 240, 240, reset=Pin(11, func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7, func=Pin.GPIO, dir=Pin.OUT))
display.init()#屏幕初始化

num=search(display, uart)
if num==1:
    print("search success")
else:
    print("search failure")
