# 基于Waffle Nano的小游戏《2048》

## 案例展示

### 主要功能介绍

&emsp;&emsp;复刻经典小游戏《2048》，使其可以在Waffle Nano上用JoyStick Shield V1.A手柄模组游玩。

![](img/inGame.jpg)

&emsp;&emsp;图为游戏中画面。

### 游戏规则介绍

&emsp;&emsp;在4×4网格中，分布着一些数字，每个数字占据一格。当执行上、下、左、右中的一个命令时，这些数字会向这个方向移动直至被阻挡。同时，在这个方向上相邻的两个相同数字会相加得到一个新的数字，相应地，其占据的位置也会减少。每移动一次，随机空位上会出现一个新数字。当所有位置全占满且没有可以合并的数字时，游戏结束。游戏得分是所有相加得到的数字之和。

### 游戏方式介绍

&emsp;&emsp;按B首次开始游戏。

&emsp;&emsp;每次开始游戏时根据提示将摇杆置于任意位置以创建随机游戏。

&emsp;&emsp;用摇杆控制四向移动。

&emsp;&emsp;游戏无法继续进行时，判定游戏结束。按C返回并开始新游戏。

## 物理连接

### 传感器选择

&emsp;&emsp; 传感器选择如下图所示的JoyStick Shield手柄模组。

![](img/joystickShield.jpg)

### 传感器接线

&emsp;&emsp;JoyStick Shield手柄模组上没有标注引脚名称。实际用到的引脚如下图所示。“摇杆引脚”中左侧为X轴的引脚，右侧为Y轴的引脚。

![](img/joystickOutput.jpg)

&emsp;&emsp;传感器与Waffle Nano之间的接线方式如下图所示。

![](img/connect.png)

## 传感器库使用

&emsp;&emsp;可以获取[joystickShield.py](/code/joystickShield.py),将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`上。

&emsp;&emsp;我们在可以在主函数中使用以下代码导入此库。

```python
from joystickShield import Cmd 
```

&emsp;&emsp;要使用库中的方法，需要定义一个`Cmd`对象。

```python
command=Cmd()
```

&emsp;&emsp;使用`Cmd`对象的`statusA()`方法可以读取出手柄模组A键的状态（0表示按下，1表示松开），此法也适用于B、C、D键。

```python
a=command.statusA()
b=command.statusB()
c=command.statusC()
d=command.statusD()
```

&emsp;&emsp;使用`Cmd`对象的`statusX()`方法可以读取出手柄模组摇杆X轴的状态（以摇杆中央位置为原点，-1表示在该轴负方向，0表示在该方向上无位移，1表示在该轴正方向），此法也适用于Y轴。

&emsp;&emsp;X轴的负方向是左，正方向是右。

&emsp;&emsp;Y轴的负方向是下，正方向是上。

```python
x=command.statusX()
y=command.statusY()
```

&emsp;&emsp;使用`Cmd`对象的`exactX()`方法可以读取出手柄模组摇杆X轴的具体位置（一个值），此法也适用于Y轴。

```python
x=command.exactX()
y=command.exactY()
```

&emsp;&emsp;关于此库相关细节说明，亦可参考代码注释。

## 案例代码复现

&emsp;&emsp;可以获取[main.py](/code/main.py)代码，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给`Waffle Nano`，以复现此案例，基础效果如下所示：

![](img/start.jpg)


&emsp;&emsp;案例相关细节说明详见代码注释.

&emsp;&emsp;也可以获取[testCode.py](/code/testCode.py)代码，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给`Waffle Nano`，运行时输出所有方法的结果，可以测试库文件[joystickShield.py](/code/joystickShield.py)的功能完整性。