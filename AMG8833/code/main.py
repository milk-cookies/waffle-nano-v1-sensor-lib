from amg8833 import AMG8833 #导入AMG8833库
from machine import I2C,SPI, Pin #导入通信以及引脚相关的库
import st7789 #导入屏幕驱动库
import utime
i2c=  I2C(1, scl=Pin(1), sda=Pin(0), freq=400000) #构造IIC对象
ircamera=AMG8833(i2c) #构造红外摄像机对象

spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8)) #构造spi对象
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT)) #构造屏幕控制对象
display.init() #屏幕初始化

backgroud_clr = st7789.color565(255, 255, 255) #设置背景颜色为白色
obj_clr = st7789.color565(0, 0, 0) #温度显示用颜色
display.fill(backgroud_clr) #屏幕背景颜色填充

obj_len = 30 #每个像素的屏幕显示尺寸
user_rows = 8 #每列8像素
user_columns = 8 #每行8像素

while True:
    IRdata=ircamera.read() #从红外照相机中获取数据
    print(IRdata)

    # 将温度转化为颜色像素，显示在屏幕上
    x=0
    y=0
    for i in range(user_rows):
        for j in range(user_columns):
            if(IRdata[i*8+j]>27.5):
                obj_clr = st7789.color565(255, 0, 0)
            elif(IRdata[i*8+j]<=27.5 and IRdata[i*8+j]>25.5):
                obj_clr = st7789.color565(0, 255, 0)
            elif(IRdata[i*8+j]<=25.5):
                obj_clr = st7789.color565(0, 0, 255)
            display.fill_rect(x,y,obj_len,obj_len, obj_clr)
            x += obj_len
        x = 0
        y += obj_len