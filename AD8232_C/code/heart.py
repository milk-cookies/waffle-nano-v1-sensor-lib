from machine import Pin,SPI,ADC
import utime
import st7789
def init():
    spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
    display = st7789.ST7789(spi, 240, 240, reset=Pin(11, func=Pin.GPIO, dir=Pin.OUT),dc=Pin(7, func=Pin.GPIO, dir=Pin.OUT))
    display.init()  # 屏幕初始化
    display.fill(st7789.color565(255, 255, 255))  # 设置屏幕界面为白色
    return display

def screen_begin(display):
    display.fill_rect(20, 200, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(25, 200, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(30, 200, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(35, 190, 5, 15, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(40, 180, 5, 10, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(45, 175, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(50, 180, 5, 35, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(55, 215, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(60, 200, 5, 15, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(65, 190, 5, 10, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(70, 185, 5, 5, st7789.color565(0, 0, 0))  # 心的左顶点
    display.fill_rect(70, 145, 5, 10, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(75, 190, 5, 20, st7789.color565(0, 0, 0))
    display.fill_rect(75, 155, 5, 10, st7789.color565(255, 0, 0))  #
    display.fill_rect(75, 135, 5, 10, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(80, 210, 5, 5, st7789.color565(0, 0, 0))
    display.fill_rect(80, 165, 5, 5, st7789.color565(255, 0, 0))  #
    display.fill_rect(80, 130, 5, 5, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(85, 200, 5, 10, st7789.color565(0, 0, 0))
    display.fill_rect(85, 170, 5, 5, st7789.color565(255, 0, 0))  #
    display.fill_rect(85, 130, 5, 5, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(90, 200, 5, 5, st7789.color565(0, 0, 0))
    display.fill_rect(90, 175, 5, 5, st7789.color565(255, 0, 0))  #
    display.fill_rect(90, 125, 5, 5, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(95, 200, 5, 5, st7789.color565(0, 0, 0))
    display.fill_rect(95, 180, 5, 5, st7789.color565(255, 0, 0))  #
    display.fill_rect(95, 125, 5, 5, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(100, 200, 5, 5, st7789.color565(0, 0, 0))
    display.fill_rect(100, 185, 5, 5, st7789.color565(255, 0, 0))  #
    display.fill_rect(100, 125, 5, 5, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(105, 190, 5, 15, st7789.color565(0, 0, 0))  # 心的左起点
    display.fill_rect(105, 185, 5, 5, st7789.color565(255, 0, 0))  #
    display.fill_rect(105, 130, 5, 10, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(110, 140, 5, 5, st7789.color565(255, 0, 0))  # 心的中心点
    utime.sleep(0.1)
    display.fill_rect(115, 190, 5, 15, st7789.color565(0, 0, 0))  # 心的右起点
    display.fill_rect(115, 185, 5, 5, st7789.color565(255, 0, 0))  #
    display.fill_rect(115, 130, 5, 10, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(120, 200, 5, 5, st7789.color565(0, 0, 0))
    display.fill_rect(120, 185, 5, 5, st7789.color565(255, 0, 0))  #
    display.fill_rect(120, 125, 5, 5, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(125, 200, 5, 5, st7789.color565(0, 0, 0))
    display.fill_rect(125, 180, 5, 5, st7789.color565(255, 0, 0))  #
    display.fill_rect(125, 125, 5, 5, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(130, 200, 5, 5, st7789.color565(0, 0, 0))
    display.fill_rect(130, 175, 5, 5, st7789.color565(255, 0, 0))  #
    display.fill_rect(130, 125, 5, 5, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(135, 200, 5, 10, st7789.color565(0, 0, 0))
    display.fill_rect(135, 170, 5, 5, st7789.color565(255, 0, 0))  #
    display.fill_rect(135, 130, 5, 5, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(140, 210, 5, 5, st7789.color565(0, 0, 0))
    display.fill_rect(140, 165, 5, 5, st7789.color565(255, 0, 0))  #
    display.fill_rect(140, 130, 5, 5, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(145, 200, 5, 10, st7789.color565(0, 0, 0))
    display.fill_rect(145, 155, 5, 10, st7789.color565(255, 0, 0))  #
    display.fill_rect(145, 135, 5, 10, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(150, 195, 5, 5, st7789.color565(0, 0, 0))  # 心的右顶点
    display.fill_rect(150, 145, 5, 10, st7789.color565(255, 0, 0))  #
    utime.sleep(0.1)
    display.fill_rect(155, 200, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(160, 190, 5, 10, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(165, 175, 5, 15, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(170, 170, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(175, 175, 5, 35, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(180, 210, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(185, 200, 5, 10, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(190, 195, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(195, 195, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(200, 185, 5, 10, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(205, 180, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(210, 185, 5, 20, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(215, 200, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.fill_rect(220, 200, 5, 5, st7789.color565(0, 0, 0))
    utime.sleep(0.1)
    display.draw_string(80, 60, "Welcome!", size=2, color=st7789.color565(0, 0, 0))
    display.draw_string(10, 90, "Send ", size=2, color=st7789.color565(0, 0, 0))
    display.draw_string(60, 90, "start", size=2, color=st7789.color565(255, 0, 0))
    display.draw_string(120, 90, "to measure!", size=2, color=st7789.color565(0, 0, 0))
    utime.sleep(5)

def screen_form(display):
    x = 0
    y = 0
    while True:  # 绘制网格界面
        display.vline(x, 0, 240, st7789.color565(228, 232, 170))
        display.hline(0, y, 240, st7789.color565(228, 232, 170))
        x = x + 30
        y = y + 30
        if (x >= 240):
            break

def screen_little_heart_1(display):
    display.fill_rect(25, 115, 5, 5, st7789.color565(239, 91, 91))
    display.fill_rect(30, 110, 5, 15, st7789.color565(239, 91, 91))
    display.fill_rect(35, 115, 5, 15, st7789.color565(239, 91, 91))
    display.fill_rect(40, 110, 5, 15, st7789.color565(239, 91, 91))
    display.fill_rect(45, 115, 5, 5, st7789.color565(239, 91, 91))

def screen_little_heart_2(display):
    display.fill_rect(190, 115, 5, 5, st7789.color565(239, 91, 91))
    display.fill_rect(195, 110, 5, 15, st7789.color565(239, 91, 91))
    display.fill_rect(200, 115, 5, 15, st7789.color565(239, 91, 91))
    display.fill_rect(205, 110, 5, 15, st7789.color565(239, 91, 91))
    display.fill_rect(210, 115, 5, 5, st7789.color565(239, 91, 91))