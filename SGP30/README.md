# 空气质量测量仪

## 案例展示

&emsp; &emsp;一个空气质量测量仪，探测空气中CO2与TVOC的含量，并将其以文字形式表现出来。

![](/SGP30/img/anli.jpg)

## 物理连接

### 传感器选择

&emsp; &emsp;传感器选择如下图所示的型号为SGP30的气体传感器模块。

![](/SGP30/img/chuanganqi.jpg)

### 传感器接线

&emsp; &emsp;传感器与Waffle Nano之间的接线方式如下表所示。

## 传感器库使用

&emsp; &emsp;可以获取sgp30.py将此库通过Waffle Maker的文件上传功能将此库上传到Waffle Nano上。

&emsp; &emsp;我们在可以在主函数中使用以下代码导入此库。

```python
from sgp30 import SGP30
```

&emsp; &emsp;我们使用空气质量测量仪对象的read（）方法读取出数据

```python
yq=SGP30(i2c)
```

&emsp; &emsp;关于此库相关细节说明详见代码注释

## 案例代码复现

&emsp; &emsp;可以获取main.py代码，将其内容复制到Waffle Maker编辑器上传输给Waffle Nano，以复现此案

&emsp; &emsp;案例相关细节说明详见代码注释


