# 定义循环队列
class Queue:
    def __init__(this, maximum_size):
        this.maximum_size = maximum_size
        this.front = 0
        this.rear = 0

        this.element = []

        for i in range(this.maximum_size+1):
            this.element.append((-1, -1))

    def empty(this):
        return this.front == this.rear

    def full(this):
        return (this.rear + 1) % this.maximum_size == this.front

    def length(this):
        return (this.rear - this.front + this.maximum_size) % this.maximum_size

    def clear(this):
        this.front = 0
        this.rear = 0

        for i in range(this.maximum_size+1):
            this.element[i] = (-1, -1)

    def get_front(this):
        return this.element[(this.front + 1) % this.maximum_size]

    def get_rear(this):
        return this.element[this.rear]

    def enqueue(this, element):
        this.rear = (this.rear+1) % this.maximum_size
        this.element[this.rear] = element

    def dequeue(this):
        this.element[(this.front + 1) % this.maximum_size] = (-1, -1)
        this.front = (this.front+1) % this.maximum_size
