# 如何贡献代码

&emsp;&emsp;我们首先fork此仓库。

![](assets/image/1.jpg)

&emsp;&emsp;然后选择需要fork到的组织或者个人。

![](assets/image/2.jpg)

&emsp;&emsp;接着系统就会自动在此组织或者个人里新建一个一模一样的项目。

![](assets/image/3.jpg)

&emsp;&emsp;然后我们像往常一样将此个人项目克隆到本地。

![](assets/image/4.jpg)

![](assets/image/5.jpg)

&emsp;&emsp;接着我们需要设置 `upstream`,我们首先复制原始项目的仓库地址。

![](assets/image/6.jpg)

&emsp;&emsp;然后使用`git remote add upstream https://gitee.com/blackwalnutlabs/waffle-nano-v1-sensor-lib.git`来设置原始项目仓库。

![](assets/image/7.jpg)

&emsp;&emsp;在提交代码之前我们先`git fetch upstream`保证此仓库内容与原始项目仓库内容相同。

&emsp;&emsp;经过 `git add` `git commit` 后`git push`此项目。

&emsp;&emsp;我们去自己的fork出来的仓库里`pull Requests`。

![](assets/image/9.jpg)

&emsp;&emsp;添加相关必要描述后选择创建。之后就是漫长地等待管理员采纳我们的提交。

![](assets/image/10.jpg)
